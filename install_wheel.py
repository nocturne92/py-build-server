#!/usr/bin/env python

# install_wheel.py -d <directory> <wheel>
# 
# script for deploying wheel files to a pypi directory
# verifies <wheel> and installs it to <directory>/<package_name>/<wheel>
#
# depends on:
#   plumbum >1.6,<1.7

import os
import re
from argparse import ArgumentParser
from subprocess import list2cmdline
from zipfile import ZipFile

from pip.wheel import Wheel
from pip.exceptions import InvalidWheelFilename
from plumbum import local
from plumbum.commands.processes import ProcessExecutionError

class InvalidWheel(Exception): pass

def parse_wheel_meta(meta):
    kv_regexp = re.compile(r'^([^:]+):\s*(.*)$', re.M)

    return {k: v for k,v in kv_regexp.findall(meta)}

def get_wheel_meta(filename):
    try:
        wheel = Wheel(os.path.basename(filename))
    except InvalidWheelFilename as err:
        raise InvalidWheel(err)

    zipfile = ZipFile(filename)

    meta_path = '{}-{}.dist-info/METADATA'.format(
        wheel.name.replace('-', '_'),
        wheel.version.replace('-', '_'))

    if not meta_path in zipfile.namelist():
        raise InvalidWheel('METADATA not found')

    return parse_wheel_meta(zipfile.read(meta_path))

def check_wheel_meta(meta):
    if meta['Name'] != wheel.name:
        raise InvalidWheel("package name doesn't match")
    if meta['Version'] != wheel.version:
        raise InvalidWheel("package version doesn't match")
            
if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('-d', '--directory', dest='directory')
    parser.add_argument('wheel')

    arg = parser.parse_args()

    assert os.path.isdir(arg.directory)
    assert os.path.isfile(arg.wheel)

    wheel = Wheel(os.path.basename(arg.wheel))

    print('verifying METADATA...')
    meta = get_wheel_meta(arg.wheel)

    if meta['Name'] != wheel.name:
        raise InvalidWheel("package name doesn't match")
    if meta['Version'] != wheel.version:
        raise InvalidWheel("package version doesn't match")

    print('package')
    print("  name: {}".format(meta['Name']))
    print("  version: {}".format(meta['Version']))
    print("")

    try:
        package_path = os.path.join(arg.directory, wheel.name)
        pip_cmd = local['pip']['download', '--no-deps', '-d', os.path.join(arg.directory, wheel.name), arg.wheel]
        print('running pip...')
        print("  " + list2cmdline(pip_cmd.formulate()))
        print("")
        res = local['pip']['download', '--no-deps', '-d', package_path, arg.wheel]
        print('{} installed to {}'.format(arg.wheel, package_path))
    except ProcessExecutionError:
        print('pip failed')
        exit(1)
